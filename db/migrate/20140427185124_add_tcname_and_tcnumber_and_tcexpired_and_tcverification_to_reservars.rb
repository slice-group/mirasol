class AddTcnameAndTcnumberAndTcexpiredAndTcverificationToReservars < ActiveRecord::Migration
  def change
    add_column :reservars, :tcname, :string
    add_column :reservars, :tcnumber, :string
    add_column :reservars, :tcexpired, :string
    add_column :reservars, :tcverification, :string
  end
end
