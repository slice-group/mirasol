class CreateEventos < ActiveRecord::Migration
  def change
    create_table :eventos do |t|
      t.string :nombre
      t.string :cedula
      t.string :empresa
      t.string :fecha_solicitud
      t.string :email
      t.string :telefono
      t.string :salon
      t.string :tipo_evento
      t.string :cantidad_personas
      t.string :fecha_inicio
      t.string :fecha_fin
      t.string :hora_inicio
      t.string :hora_culminacion
      t.string :tipo_montaje
      t.text :descripcion_evento
      t.boolean :pasapalos_frios
      t.boolean :pasapalos_calientes
      t.boolean :menu_plateado
      t.boolean :tipo_buffet
      t.boolean :estaciones_tematicas
      t.boolean :coffeebreak_matutino
      t.boolean :coffeebreak_vespertino
      t.boolean :mesa_quesos
      t.boolean :descorche_cocteles
      t.boolean :descorche_whisky
      t.boolean :descorche_vino
      t.boolean :descorche_vodka
      t.boolean :barra_coctelera
      t.boolean :refrescos
      t.string :colores_motivos
      t.boolean :arreglos_florales
      t.boolean :globos
      t.boolean :toldo
      t.boolean :sonido
      t.boolean :dj
      t.boolean :grupo_musical
      t.boolean :podium
      t.boolean :videobeam
      t.boolean :iluminacion
      t.boolean :imflables
      t.boolean :show_magia
      t.boolean :show_baile
      t.boolean :hora_loca
      t.boolean :barman
      t.boolean :animacion_infantil
      t.boolean :mesoneros
      t.text :otros_servicios
      t.text :observaciones_adicionales

      t.timestamps
    end
  end
end
