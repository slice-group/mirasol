class CreateReservars < ActiveRecord::Migration
  def change
    create_table :reservars do |t|
      t.string :nombre
      t.string :cedula
      t.string :email
      t.string :telefono
      t.string :adults
      t.string :kids
      t.string :check_in
      t.string :check_out
      t.string :habitacion

      t.timestamps
    end
  end
end
