# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140427205345) do

  create_table "eventos", force: true do |t|
    t.string   "nombre"
    t.string   "cedula"
    t.string   "empresa"
    t.string   "fecha_solicitud"
    t.string   "email"
    t.string   "telefono"
    t.string   "salon"
    t.string   "tipo_evento"
    t.string   "cantidad_personas"
    t.string   "fecha_inicio"
    t.string   "fecha_fin"
    t.string   "hora_inicio"
    t.string   "hora_culminacion"
    t.string   "tipo_montaje"
    t.text     "descripcion_evento"
    t.boolean  "pasapalos_frios"
    t.boolean  "pasapalos_calientes"
    t.boolean  "menu_plateado"
    t.boolean  "tipo_buffet"
    t.boolean  "estaciones_tematicas"
    t.boolean  "coffeebreak_matutino"
    t.boolean  "coffeebreak_vespertino"
    t.boolean  "mesa_quesos"
    t.boolean  "descorche_cocteles"
    t.boolean  "descorche_whisky"
    t.boolean  "descorche_vino"
    t.boolean  "descorche_vodka"
    t.boolean  "barra_coctelera"
    t.boolean  "refrescos"
    t.string   "colores_motivos"
    t.boolean  "arreglos_florales"
    t.boolean  "globos"
    t.boolean  "toldo"
    t.boolean  "sonido"
    t.boolean  "dj"
    t.boolean  "grupo_musical"
    t.boolean  "podium"
    t.boolean  "videobeam"
    t.boolean  "iluminacion"
    t.boolean  "imflables"
    t.boolean  "show_magia"
    t.boolean  "show_baile"
    t.boolean  "hora_loca"
    t.boolean  "barman"
    t.boolean  "animacion_infantil"
    t.boolean  "mesoneros"
    t.text     "otros_servicios"
    t.text     "observaciones_adicionales"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reservars", force: true do |t|
    t.string   "nombre"
    t.string   "cedula"
    t.string   "email"
    t.string   "telefono"
    t.string   "adults"
    t.string   "kids"
    t.string   "check_in"
    t.string   "check_out"
    t.string   "habitacion"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "procedencia"
    t.string   "motivo"
    t.integer  "canthab"
    t.string   "metodo_pago"
    t.string   "tcname"
    t.string   "tcnumber"
    t.string   "tcexpired"
    t.string   "tcverification"
    t.string   "tctype"
  end

end
