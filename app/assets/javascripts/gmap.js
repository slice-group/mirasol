var map;
var markersArray = [];
var latlng = new google.maps.LatLng(11.7353716,-70.181914,18);

function initialize()
{  
  var myOptions = {
      zoom: 16,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      scrollwheel: false
  };
  
  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
  placeMarker(latlng);
}

function placeMarker(location) {
    
	var marker = new google.maps.Marker({
	    position: location, 
	    map: map,
	    animation: google.maps.Animation.BOUNCE,
	    title:"Mirasol Suites, Punto Fijo, Falcón, Venezuela",
	    icon: "/assets/favicon.png"
	});

	// add marker in markers array
	markersArray.push(marker);

	//map.setCenter(location);
}


google.maps.event.addDomListener(window, 'load', initialize);