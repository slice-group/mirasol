module ReservarsHelper
	def select_habitacion(f)
		f.select :habitacion, options_for_select(['Hab. Estándar', 'Hab. Doble', 'Suite Estándar', 'Suite Junior', 'Suite Deluxe']), { include_blank: "#{t('module.reservaciones.seleccionar_hab')}" }, { class: "contactos-input", name: "reservar[habitacion][]" }
	end
end
