class StaticpageController < ApplicationController
  def index
  end

  def hotel
  end

  def habitaciones
  end

  def salones
  end

  def contacto
  end

  def enviar_contacto
    if verify_recaptcha(attribute: "contact", message: "Oh! It's error with reCAPTCHA!")
      ContactMailer.contact(params).deliver
      redirect_to staticpage_index_path, notice: params[:name]+', ¡Tu mensaje ha sido en enviado!'
    else
      redirect_to staticpage_contacto_path
    end
  end

  def restoran
  end

  def eventos 
     redirect_to new_evento_path
  end

  def reservas 
    redirect_to reservation_reservar_path cin: params[:check_in].gsub("/","-"), cout: params[:check_out].gsub("/","-")
  end

  def ver_habitaciones   
    @habitacion = params[:id]
  end
end
