class EventosController < ApplicationController
  before_action :set_evento, only: [:show, :edit, :update, :destroy]

  # GET /eventos
  # GET /eventos.json
  def index
    @eventos = Evento.all
  end

  # GET /eventos/1
  # GET /eventos/1.json
  def show
  end

  # GET /eventos/new
  def new
    @evento = Evento.new(fecha_inicio: DateTime.now.strftime("%d/%m/%Y"), fecha_fin: DateTime.now.strftime("%d/%m/%Y"))
  end

  # GET /eventos/1/edit
  def edit
  end

  # POST /eventos
  # POST /eventos.json
  def create
    params[:evento].parse_time_select!(:hora_inicio)
    params[:evento].parse_time_select!(:hora_culminacion)
    if !evento_params[:hora_inicio].to_s.empty? && !evento_params[:hora_culminacion].to_s.empty?
      params[:evento][:hora_inicio] = params[:evento][:hora_inicio].strftime("%I:%M %p")
      params[:evento][:hora_culminacion] = params[:evento][:hora_culminacion].strftime("%I:%M %p")
    end
    @evento = Evento.new(evento_params)

    respond_to do |format|
      if @evento.valid?
        EventoMailer.eventos_email(@evento).deliver
        format.html { redirect_to staticpage_index_path, notice: @evento.nombre+', La solicitud de reserva para su evento ya ha sido enviada' }
        format.json { render action: 'show', status: :created, location: @evento }
      else
        format.html { render action: 'new' }
        format.json { render json: @evento.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /eventos/1
  # PATCH/PUT /eventos/1.json
  def update
    respond_to do |format|
      if @evento.update(evento_params)
        format.html { redirect_to @evento, notice: 'Evento was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @evento.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /eventos/1
  # DELETE /eventos/1.json
  def destroy
    @evento.destroy
    respond_to do |format|
      format.html { redirect_to eventos_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_evento
      @evento = Evento.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def evento_params
      params.require(:evento).permit(:nombre, :cedula, :empresa, :fecha_solicitud, :email, :telefono, :salon, :tipo_evento, :cantidad_personas, :fecha_inicio, :fecha_fin, :hora_inicio, :hora_culminacion, :tipo_montaje, :descripcion_evento, :pasapalos_frios, :pasapalos_calientes, :menu_plateado, :tipo_buffet, :estaciones_tematicas, :coffeebreak_matutino, :coffeebreak_vespertino, :mesa_quesos, :descorche_cocteles, :descorche_whisky, :descorche_vino, :descorche_vodka, :barra_coctelera, :refrescos, :colores_motivos, :arreglos_florales, :globos, :toldo, :sonido, :dj, :grupo_musical, :podium, :videobeam, :iluminacion, :imflables, :show_magia, :show_baile, :hora_loca, :barman, :animacion_infantil, :mesoneros, :otros_servicios, :observaciones_adicionales)
    end
end
