class ReservarsController < ApplicationController
  before_action :set_reservar, only: [:show, :edit, :update, :destroy]
  # GET /reservars
  # GET /reservars.json
  def index
    @reservars = Reservar.all
  end

  # GET /reservars/1
  # GET /reservars/1.json
  def show
  end

  # GET /reservars/new
  def new_date
    @reservar = Reservar.new(check_in: params[:cin].gsub("-", "/"), check_out: params[:cout].gsub("-", "/"))
  end

  def new
    cout = DateTime.now+1
    @reservar = Reservar.new(check_in: DateTime.now.strftime("%d/%m/%Y"), check_out: cout.strftime("%d/%m/%Y"))
  end

  # GET /reservars/1/edit
  def edit
  end

  # POST /reservars
  # POST /reservars.json
  def create
    if params[:reservar][:metodo_pago] == "Tarjeta de Crédito"
      params[:reservar].parse_time_select!(:tcexpired)
      params[:reservar][:tcexpired] = params[:reservar][:tcexpired].strftime("%m/%Y")
    end
    @reservar = Reservar.new(reservar_params)

    respond_to do |format|
      if @reservar.valid?
        ReservarMailer.reservation(@reservar).deliver
        format.html { redirect_to staticpage_index_path, notice: @reservar.nombre+', Su solicitud de reserva debe ser confirmada, nos comunicaremos con usted pronto vía e-mail.' }
        format.json { render action: 'show', status: :created, location: @reservar }
      else
        format.html { render action: 'new' }
        format.json { render json: @reservar.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reservars/1
  # PATCH/PUT /reservars/1.json
  def update
    respond_to do |format|
      if @reservar.update(reservar_params)
        format.html { redirect_to @reservar, notice: 'Reservar was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @reservar.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reservars/1
  # DELETE /reservars/1.json
  def destroy
    @reservar.destroy
    respond_to do |format|
      format.html { redirect_to reservars_url }
      format.json { head :no_content }
    end
  end

  private
    
    # Use callbacks to share common setup or constraints between actions.
    def set_reservar
      @reservar = Reservar.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reservar_params
      params.require(:reservar).permit(:nombre, :cedula, :email, :telefono, :adults, :procedencia, :motivo, :kids, :check_in, :check_out, {:habitacion => []}, :canthab, :metodo_pago, :tcname, :tcnumber, :tcexpired, :tcverification, :tctype)
    end
end
