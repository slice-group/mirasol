json.array!(@reservars) do |reservar|
  json.extract! reservar, :id, :nombre, :cedula, :email, :telefono, :adults, :kids, :check_in, :check_out, :habitacion
  json.url reservar_url(reservar, format: :json)
end
