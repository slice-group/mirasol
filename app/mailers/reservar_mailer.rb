class ReservarMailer < ActionMailer::Base
  default from: "contactoshms@gmail.com"
 
  def reservation(reservacion)
  	@reservacion = reservacion
    mail(to: "reservaciones@mirasolsuites.com", subject: reservacion.nombre+' ha solicitado una reservación.')
  end
end
