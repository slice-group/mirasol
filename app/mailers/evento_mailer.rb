class EventoMailer < ActionMailer::Base
  default from: "contactoshms@gmail.com"

  def eventos_email(evento)
  	@evento = evento
  	mail(to:"ventas@mirasolsuites.com", subject: evento.nombre+" ha solicitado una reservación para un evento.")
  end
end
