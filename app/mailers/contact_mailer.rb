class ContactMailer < ActionMailer::Base
	default from: "contactoshms@gmail.com"

  def contact(contacto)
    @name = contacto[:name]
    @email = contacto[:email]
    @comment = contacto[:comment]
    #mail(to: "info@mirasolsuites.com", subject: @name+' ha contactado con Mirasol.')
    mail(to: "info@mirasolsuites.com", subject: @name+' ha contactado con Mirasol.')
  end
end
