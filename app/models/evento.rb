class Evento < ActiveRecord::Base
	validates_presence_of :nombre, :cedula, :email, :telefono, :salon, :fecha_inicio, :fecha_fin, :tipo_montaje
	#validacion de cedula
	validates :cedula, :length => { :minimum => 7, :maximum => 8 }
	validates_numericality_of :cedula, :only_integer => true

	#validaciones para telefono
  	validates :telefono, :length => { :minimum => 11, :maximum => 13 }
  	validates_numericality_of :telefono, :only_integer => true

	#validacion de email
	validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create }

	#validacion de cantidad de personas
	validates :cantidad_personas, :numericality => { :less_than_or_equal_to => 100 }

	#custom validate
	validate :fecha_de_salida_mayor_a_fecha_de_entrada?

	def opcion_seleccionada(seleccion)
		if seleccion == true
			return "Si"
		else
			return "No"
		end
	end

	def fecha_de_salida_mayor_a_fecha_de_entrada?
		cin = Date.strptime(self.fecha_inicio, "%d/%m/%Y")
		cout = Date.strptime(self.fecha_fin, "%d/%m/%Y")
		if cout < cin
			errors.add(:fecha_fin, "No puede ser menor a la fecha de entrada")
		end
	end

end
