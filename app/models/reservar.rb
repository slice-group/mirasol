class Reservar < ActiveRecord::Base
	before_validation :data_habitacion
	validates_presence_of :nombre, :cedula, :telefono, :adults, :check_in, :check_out, :email, :procedencia, :motivo, :canthab
	
	#validacion de nombre
	validates :nombre, :length => { :maximum => 35 }

	#validacion de cedula
	validates :cedula, :length => { :minimum => 7, :maximum => 8 }
	validates_numericality_of :cedula, :only_integer => true

	#validaciones para telefono
  validates :telefono, :length => { :minimum => 11, :maximum => 13 }
  validates_numericality_of :telefono, :only_integer => true

  #validaciones para numero de tarjeta
  validates :tcnumber, :length => { :minimum => 16, :maximum => 16 }, :allow_blank => true
  validates_numericality_of :tcnumber, :only_integer => true, :allow_blank => true

  #validaciones para numero de verificacion
  validates :tcverification, :length => { :minimum => 3, :maximum => 3 }, :allow_blank => true
  validates_numericality_of :tcverification, :only_integer => true, :allow_blank => true

	#validacion de email
	validates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create }

	#custom validate
	validate :fecha_de_salida_mayor_a_fecha_de_entrada?
	validate :reservacion_por_una_semana?
	#validate :habitacion_check_in_disponible?, :habitacion_check_out_disponible? validar fechas por rango

	def fecha_de_salida_mayor_a_fecha_de_entrada?
		cin = Date.strptime(self.check_in, "%d/%m/%Y")
		cout = Date.strptime(self.check_out, "%d/%m/%Y")
		if cout < cin
			errors.add(:check_out, "No puede ser menor a la fecha de entrada")
		end
	end

	def reservacion_por_una_semana?
		cin = Date.strptime(self.check_in, "%d/%m/%Y")
		cout = Date.strptime(self.check_out, "%d/%m/%Y")
		if cout-cin < 1 
			errors.add(:check_out, "Debe reservar como minimo una noche")
		end
	end

	def habitacion_check_in_disponible?
		interval = { inicio: "15/04/2014", fin: "20/04/2014" }
		if Date.strptime(self.check_in, "%d/%m/%Y") >= Date.strptime(interval[:inicio], "%d/%m/%Y") and Date.strptime(self.check_in, "%d/%m/%Y") <= Date.strptime(interval[:fin], "%d/%m/%Y") and self.habitacion == "Suite Deluxe"
			errors.add(:check_in, "No hay habitaciones SUITE DELUXE disponibles para las fechas comprendidas desde #{interval[:inicio]} al #{interval[:fin]}")
		end
	end

	def habitacion_check_out_disponible?
		interval = { inicio: "15/04/2014", fin: "20/04/2014" }
		if Date.strptime(self.check_out, "%d/%m/%Y") >= Date.strptime(interval[:inicio], "%d/%m/%Y") and Date.strptime(self.check_out, "%d/%m/%Y") <= Date.strptime(interval[:fin], "%d/%m/%Y") and self.habitacion == "Suite Deluxe"
			errors.add(:check_out, "No hay habitaciones SUITE DELUXE disponibles para las fechas comprendidas desde #{interval[:inicio]} al #{interval[:fin]}")
		end
	end

	def data_habitacion
		self.habitacion = self.habitacion
	end
end
