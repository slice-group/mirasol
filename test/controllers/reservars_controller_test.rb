require 'test_helper'

class ReservarsControllerTest < ActionController::TestCase
  setup do
    @reservar = reservars(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:reservars)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create reservar" do
    assert_difference('Reservar.count') do
      post :create, reservar: { adults: @reservar.adults, cedula: @reservar.cedula, check_in: @reservar.check_in, check_out: @reservar.check_out, email: @reservar.email, habitacion: @reservar.habitacion, kids: @reservar.kids, nombre: @reservar.nombre, telefono: @reservar.telefono }
    end

    assert_redirected_to reservar_path(assigns(:reservar))
  end

  test "should show reservar" do
    get :show, id: @reservar
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @reservar
    assert_response :success
  end

  test "should update reservar" do
    patch :update, id: @reservar, reservar: { adults: @reservar.adults, cedula: @reservar.cedula, check_in: @reservar.check_in, check_out: @reservar.check_out, email: @reservar.email, habitacion: @reservar.habitacion, kids: @reservar.kids, nombre: @reservar.nombre, telefono: @reservar.telefono }
    assert_redirected_to reservar_path(assigns(:reservar))
  end

  test "should destroy reservar" do
    assert_difference('Reservar.count', -1) do
      delete :destroy, id: @reservar
    end

    assert_redirected_to reservars_path
  end
end
