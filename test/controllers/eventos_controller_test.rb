require 'test_helper'

class EventosControllerTest < ActionController::TestCase
  setup do
    @evento = eventos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:eventos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create evento" do
    assert_difference('Evento.count') do
      post :create, evento: { animacion_infantil: @evento.animacion_infantil, arreglos_florales: @evento.arreglos_florales, barman: @evento.barman, barra_coctelera: @evento.barra_coctelera, cantidad_personas: @evento.cantidad_personas, cedula: @evento.cedula, coffeebreak_matutino: @evento.coffeebreak_matutino, coffeebreak_vespertino: @evento.coffeebreak_vespertino, colores_motivos: @evento.colores_motivos, descorche_cocteles: @evento.descorche_cocteles, descorche_vino: @evento.descorche_vino, descorche_vodka: @evento.descorche_vodka, descorche_whisky: @evento.descorche_whisky, descripcion_evento: @evento.descripcion_evento, dj: @evento.dj, email: @evento.email, empresa: @evento.empresa, estaciones_tematicas: @evento.estaciones_tematicas, fecha_fin: @evento.fecha_fin, fecha_inicio: @evento.fecha_inicio, fecha_solicitud: @evento.fecha_solicitud, globos: @evento.globos, grupo_musical: @evento.grupo_musical, hora_culminacion: @evento.hora_culminacion, hora_inicio: @evento.hora_inicio, hora_loca: @evento.hora_loca, iluminacion: @evento.iluminacion, imflables: @evento.imflables, menu_plateado: @evento.menu_plateado, mesa_quesos: @evento.mesa_quesos, mesoneros: @evento.mesoneros, nombre: @evento.nombre, observaciones_adicionales: @evento.observaciones_adicionales, otros_servicios: @evento.otros_servicios, pasapalos_calientes: @evento.pasapalos_calientes, pasapalos_frios: @evento.pasapalos_frios, podium: @evento.podium, refrescos: @evento.refrescos, salon: @evento.salon, show_baile: @evento.show_baile, show_magia: @evento.show_magia, sonido: @evento.sonido, telefono: @evento.telefono, tipo_buffet: @evento.tipo_buffet, tipo_evento: @evento.tipo_evento, tipo_montaje: @evento.tipo_montaje, toldo: @evento.toldo, videobeam: @evento.videobeam }
    end

    assert_redirected_to evento_path(assigns(:evento))
  end

  test "should show evento" do
    get :show, id: @evento
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @evento
    assert_response :success
  end

  test "should update evento" do
    patch :update, id: @evento, evento: { animacion_infantil: @evento.animacion_infantil, arreglos_florales: @evento.arreglos_florales, barman: @evento.barman, barra_coctelera: @evento.barra_coctelera, cantidad_personas: @evento.cantidad_personas, cedula: @evento.cedula, coffeebreak_matutino: @evento.coffeebreak_matutino, coffeebreak_vespertino: @evento.coffeebreak_vespertino, colores_motivos: @evento.colores_motivos, descorche_cocteles: @evento.descorche_cocteles, descorche_vino: @evento.descorche_vino, descorche_vodka: @evento.descorche_vodka, descorche_whisky: @evento.descorche_whisky, descripcion_evento: @evento.descripcion_evento, dj: @evento.dj, email: @evento.email, empresa: @evento.empresa, estaciones_tematicas: @evento.estaciones_tematicas, fecha_fin: @evento.fecha_fin, fecha_inicio: @evento.fecha_inicio, fecha_solicitud: @evento.fecha_solicitud, globos: @evento.globos, grupo_musical: @evento.grupo_musical, hora_culminacion: @evento.hora_culminacion, hora_inicio: @evento.hora_inicio, hora_loca: @evento.hora_loca, iluminacion: @evento.iluminacion, imflables: @evento.imflables, menu_plateado: @evento.menu_plateado, mesa_quesos: @evento.mesa_quesos, mesoneros: @evento.mesoneros, nombre: @evento.nombre, observaciones_adicionales: @evento.observaciones_adicionales, otros_servicios: @evento.otros_servicios, pasapalos_calientes: @evento.pasapalos_calientes, pasapalos_frios: @evento.pasapalos_frios, podium: @evento.podium, refrescos: @evento.refrescos, salon: @evento.salon, show_baile: @evento.show_baile, show_magia: @evento.show_magia, sonido: @evento.sonido, telefono: @evento.telefono, tipo_buffet: @evento.tipo_buffet, tipo_evento: @evento.tipo_evento, tipo_montaje: @evento.tipo_montaje, toldo: @evento.toldo, videobeam: @evento.videobeam }
    assert_redirected_to evento_path(assigns(:evento))
  end

  test "should destroy evento" do
    assert_difference('Evento.count', -1) do
      delete :destroy, id: @evento
    end

    assert_redirected_to eventos_path
  end
end
