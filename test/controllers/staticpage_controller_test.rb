require 'test_helper'

class StaticpageControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get hotel" do
    get :hotel
    assert_response :success
  end

  test "should get habitaciones" do
    get :habitaciones
    assert_response :success
  end

  test "should get salones" do
    get :salones
    assert_response :success
  end

  test "should get restoran" do
    get :restoran
    assert_response :success
  end

  test "should get eventos" do
    get :eventos
    assert_response :success
  end

  test "should get reservas" do
    get :reservas
    assert_response :success
  end

  test "should get contacto" do
    get :contacto
    assert_response :success
  end

end
